﻿using System;

namespace StressTesting
{
    public class TaskGroup
    {
        public readonly string Name;
        public int WorkingThreads = 0;
        public int CompletedThreads = 0;
        public TimeSpan TotalResponseTime = TimeSpan.FromSeconds(0);
        public TimeSpan AverageResponseTime => TotalResponseTime / CompletedThreads;

        public TaskGroup(string name)
        {
            Name = name;
        }
    }
}
