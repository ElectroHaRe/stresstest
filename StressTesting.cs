﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace StressTesting
{
    public class StressTesting
    {
        public event Action<string, TimeSpan> OnGettingResponse;

        public HttpClient Client { get; private set; }

        public TimeSpan TotalAverageTime => GetAverageTime();
        public int TotalWorkingThreads => CalculateWorkingThreads();
        public int TotalCompletedThreads => CalculateCompletedThreads();

        private object locker;

        public List<TaskGroup> taskGroups { get; private set; } = new List<TaskGroup>();

        public StressTesting(HttpClient client, Action<string, TimeSpan> onGettingResponse = null)
        {
            Client = client;
            OnGettingResponse += GettingResponseHandler;
            if (onGettingResponse != null)
                OnGettingResponse += onGettingResponse;
        }

        public async Task<HttpResponseMessage> PostAsync(string path, HttpContent body)
        {
            return await Client.PostAsync(path, body);
        }

        public async Task<HttpResponseMessage> GetAsync(string path)
        {
            return await Client.GetAsync(path);
        }

        public async Task StartTestAsync(Func<Task<HttpResponseMessage>> action, int cyclesCount = 1, TimeSpan freezeTime = default, string taskPrefix = "unknown")
        {
            var tasks = new List<Task>();
            for (int i = 0; i < cyclesCount; i++)
            {
                tasks.Add(Task.Run(async () => await StartTestAsync(action, taskPrefix)));

                await Task.Delay(freezeTime);
            }
            Task.WaitAll(tasks.ToArray());
        }

        public async Task StartTestAsync(IDictionary<string, Func<Task<HttpResponseMessage>>> actions, int cyclesCount = 1, TimeSpan freezeTime = default)
        {
            var tasks = new List<Task>();
            for (int i = 0; i < cyclesCount; i++)
            {
                foreach (var action in actions)
                {
                    tasks.Add(Task.Run(async () => await StartTestAsync(action.Value, action.Key)));

                }
                await Task.Delay(freezeTime);
            }
            Task.WaitAll(tasks.ToArray());
        }

        private async Task<TimeSpan> StartTestAsync(Func<Task> action, string taskPrefix = "unknown")
        {
            Stopwatch timer = new Stopwatch();

            if (!taskGroups.Exists((item) => item.Name == taskPrefix))
                taskGroups.Add(new TaskGroup(taskPrefix));
            taskGroups.Find((item) => item.Name == taskPrefix).WorkingThreads++;

            await action?.Invoke();
            timer.Stop();
            lock (locker)
            {
                OnGettingResponse?.Invoke(taskPrefix, timer.Elapsed);
            }
            return timer.Elapsed;
        }

        public TimeSpan GetAverageTime(params string[] taskPrefixCollection)
        {
            TimeSpan result = TimeSpan.FromSeconds(0);
            foreach (var taskPrefix in taskPrefixCollection)
            {
                result += taskGroups.Find((item) => item.Name == taskPrefix).AverageResponseTime;
            }
            return result;
        }

        private void GettingResponseHandler(string taskPrefix, TimeSpan time)
        {
            taskGroups.Find((item) => item.Name == taskPrefix).WorkingThreads--;
            taskGroups.Find((item) => item.Name == taskPrefix).CompletedThreads++;
            taskGroups.Find((item) => item.Name == taskPrefix).TotalResponseTime += time;
        }

        private int CalculateWorkingThreads()
        {
            int result = 0;
            foreach (var wt in taskGroups)
            {
                result += wt.WorkingThreads;
            }
            return result;
        }

        private int CalculateCompletedThreads()
        {
            int result = 0;
            foreach (var ct in taskGroups)
            {
                result += ct.CompletedThreads;
            }
            return result;
        }
    }
}
